module.exports = {
  async up(db, client) {
    await db.collection('users').updateMany({}, [
      {
        $set: {
          limitPerDay: 7,
        }
      },
      {
        $set: {
          knowsWords: [],
        }
      },
      {
        $unset: [
          'learnedWords',
          'rememberWords',
          'dailyWords',
        ]
      }
    ])
  },

  async down(db, client) {
    await db.collection('users').updateMany({}, [
      {
        $set: {
          learnedWords: [],
        }
      },
      {
        $set: {
          rememberWords: [],
        }
      },
      {
        $set: {
          dailyWords: [],
        }
      },
      {
        $unset: [
          'limitPerDay',
          'knowsWords'
        ]
      }
    ])
  }
};
