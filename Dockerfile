# Stage 1 - the build process
FROM node:latest as build-deps
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install
COPY . ./
RUN npm run build

# Stage 2 - the production environment
FROM node
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install --production
COPY --from=build-deps /usr/src/app/dist ./dist
EXPOSE 8080
CMD export GOOGLE_APPLICATION_CREDENTIALS="/usr/src/app/google-tokens.json" && npm run start:prod
