import { NestFactory } from '@nestjs/core'
import { ValidationPipe } from '@nestjs/common'
import { AppModule } from './app.module'
import { I18n } from 'telegraf-i18n'
import { User } from './users/interfaces/user.interface'

declare const module: any

declare module 'telegraf-i18n' {
  interface I18n {
    locale(languageCode: string): void
  }

  export function match(resourceKey: string, templateData?: any): string
}

declare module 'telegraf' {
  interface ContextMessageUpdate {
    i18n: I18n
    scene: any
    session: {
      language: string,
      sceneStartMessageId?: number,
      user?: User,
    }
    movie: any
    webhookReply: boolean,
  }
  interface Message {
    reply_markup?: any
  }
}

declare module 'telegram-typings' {
  interface Message {
    reply_markup?: any
  }
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true })
  app.useGlobalPipes(new ValidationPipe())
  await app.listen(3001)

  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }
}
bootstrap()
