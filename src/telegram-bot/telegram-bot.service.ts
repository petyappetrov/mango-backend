import TelegrafI18n from 'telegraf-i18n'
import Telegraf, {
  ContextMessageUpdate,
  session,
} from 'telegraf'
import { Injectable, OnModuleInit, Inject, forwardRef, OnApplicationBootstrap } from '@nestjs/common'
import { resolve } from 'path'
import { UsersService } from '../users/users.service'
import { ConfigService } from '../config.service'

@Injectable()
export class TelegramBotService
  implements OnApplicationBootstrap, OnModuleInit {
  public bot: Telegraf<ContextMessageUpdate>
  public i18n: any
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly configService: ConfigService,
  ) {}

  onModuleInit() {
    this.bot = new Telegraf(this.configService.get('TELEGRAM_BOT_TOKEN'))
    this.middlewares()
    this.errorHandler()
    this.bot.command('version', this.onlyDevelopers.bind(this), (ctx) => {
      ctx.reply('Версия: ' + process.env.npm_package_version)
    })
  }

  onApplicationBootstrap() {
    this.bot.launch()
  }

  middlewares() {
    this.i18n = new TelegrafI18n({
      defaultLanguage: 'ru',
      directory: resolve(__dirname, 'locales'),
      useSession: true,
      allowMissing: true,
    })

    this.bot.use(session())
    this.bot.use(this.i18n.middleware())
    this.bot.use(async (ctx, next) => {
      if (!ctx.session.user) {
        const user = await this.usersService.getUserByTelegram(ctx.from.id)
        ctx.session.user = user
      }
      if (!ctx.session.language && ctx.session.user) {
        ctx.session.language = ctx.session.user.nativeLanguage
        ctx.i18n.locale(ctx.session.user.nativeLanguage)
      }
      return next()
    })

  }

  errorHandler() {
    this.bot.catch((err: Error, ctx: ContextMessageUpdate) => {
      console.error(err)
      const name =  ctx.from.username || ctx.from.first_name
      const message = `By user: ${name}\nError message: ${err.message}`
      return ctx.telegram.sendMessage(
        this.configService.get('DEVELOPER_TELEGRAM_ID'),
        message,
      )
    })
  }

  public onlyDevelopers(ctx: ContextMessageUpdate, next: () => any) {
    const developer = this.configService.get('DEVELOPER_TELEGRAM_ID')
    if (developer && ctx.from.id === parseInt(developer, 10)) {
      return next()
    }
    return ctx.reply(ctx.i18n.t('MESSAGE_YOUR_ARE_NOT_DEVELOPER'))
  }
}
