import { Module, forwardRef } from '@nestjs/common'
import { TelegramBotService } from './telegram-bot.service'
import { UsersModule } from '../users/users.module'
import { WordsModule } from '../words/words.module'
import { LanguagesModule } from '../languages/languages.module'
import { ConfigModule } from '../config.module'
import { GoogleAPIModule } from '../google-api/google-api.module'
import { LevelsModule } from '../levels/levels.module'
import { Utils } from '../utils'
import { StartCommand } from './commands/start.command'
import { LevelCommand } from './commands/level.command'
import { TimezoneCommand } from './commands/timezone.command'
import { NotificationStartCommand } from './commands/notification-start.command'
import { NotificationEndCommand } from './commands/notification-end.command'
import { HelpCommand } from './commands/help.command'
import { SettingsCommand } from './commands/settings.command'
import { WordCommand } from './commands/word.command'
import { CardsChooseCommand } from './commands/cards-choose.command'
import { CardsRepeatCommand } from './commands/cards-repeat.command'

/**
 * Need enable when we add other languages
 * NativeLanguageCommand to the providers
 */
@Module({
  imports: [
    UsersModule,
    WordsModule,
    LanguagesModule,
    ConfigModule,
    GoogleAPIModule,
    LevelsModule,
  ],
  providers: [
    CardsChooseCommand,
    CardsRepeatCommand,
    WordCommand,
    TelegramBotService,
    StartCommand,
    LevelCommand,
    TimezoneCommand,
    NotificationStartCommand,
    NotificationEndCommand,
    HelpCommand,
    SettingsCommand,
    Utils,
  ],
  exports: [
    TelegramBotService,
    WordCommand,
    CardsChooseCommand,
    CardsRepeatCommand,
  ],
})

export class TelegramBotModule {}
