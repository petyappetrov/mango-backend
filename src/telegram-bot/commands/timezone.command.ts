import {
  Injectable,
  OnModuleInit,
} from '@nestjs/common'
import { ContextMessageUpdate, Stage, BaseScene, SceneContextMessageUpdate } from 'telegraf'
import { TelegramBotService } from '../telegram-bot.service'
import { GoogleAPIService } from '../../google-api/google-api.service'
import { UsersService } from '../../users/users.service'
import { UsersSchedules } from '../../users/users.schedules'
import { Utils } from '../../utils'
import { NotificationStartCommand } from './notification-start.command'

@Injectable()
export class TimezoneCommand implements OnModuleInit {
  public scene: BaseScene<SceneContextMessageUpdate>
  constructor(
    private readonly botService: TelegramBotService,
    private readonly usersService: UsersService,
    private readonly usersSchedules: UsersSchedules,
    private readonly googleApiService: GoogleAPIService,
    private readonly notificationStartCommand: NotificationStartCommand,
    private readonly utils: Utils,
  ) {}

  onModuleInit() {
    this.scene = new BaseScene('timezone')
    this.scene.enter(this.enter.bind(this))
    this.scene.on('text', async (ctx) => {
      try {
        const timezone = await this.googleApiService.getTimezoneFromAddress(ctx.message.text)

        /**
         * Update user timezone
         */
        const user = await this.usersService.setTimezoneUser(ctx.from.id, timezone.json.timeZoneId)

        /**
         * Start schedule for send message
         */
        this.usersSchedules.startSchedule(user)

        await ctx.replyWithHTML(ctx.i18n.t('SCENE_TIMEZONE_ANSWER', { timezone: timezone.json.timeZoneId }))

        /**
         * Remove starting message
         */
        if (ctx.session.sceneStartMessageId) {
          await ctx.telegram.deleteMessage(ctx.from.id, ctx.session.sceneStartMessageId)
        }

        /**
         * Remove current message
         */
        await ctx.deleteMessage()
        await ctx.scene.leave()

        if (this.usersService.userIsDisabled(user)) {
          await this.utils.wait(1000)
          this.notificationStartCommand.command(ctx)
        }
      } catch (error) {
        ctx.reply(error.message)
      }
    })

    /**
     * Create stage
     */
    const stage = new Stage([])

    stage.command('cancel', (ctx) => {
      ctx.reply(ctx.i18n.t('SCENE_TIMEZONE_LEAVE'))
      return ctx.scene.leave()
    })

    /**
     * Register stage
     */
    stage.register(this.scene)
    this.botService.bot.use(stage.middleware())

    this.botService.bot.command('timezone', (ctx) => ctx.scene.enter('timezone'))
  }

  async enter(ctx: ContextMessageUpdate) {
    const { message_id } = await ctx.reply(ctx.i18n.t('SCENE_TIMEZONE_ENTER'))
    ctx.session.sceneStartMessageId = message_id
  }
}
