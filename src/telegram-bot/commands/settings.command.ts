import {
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common'
import { UsersService } from '../../users/users.service'
import { TelegramBotService } from '../telegram-bot.service'
import { ContextMessageUpdate } from 'telegraf'
import { LanguagesService } from '../../languages/languages.service'
import { LevelsService } from '../../levels/levels.service'

@Injectable()
export class SettingsCommand implements OnApplicationBootstrap {
  constructor(
    private readonly usersService: UsersService,
    private readonly languagesService: LanguagesService,
    private readonly levelService: LevelsService,
    private readonly botService: TelegramBotService,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command('settings', this.command.bind(this))
  }

  public async command(ctx: ContextMessageUpdate) {
    const user = await this.usersService.getUserByTelegram(ctx.from.id)
    const messageValues: any = {
      timezone: user.timezone,
      notificationsStart: user.notificationsStart,
      notificationsEnd: user.notificationsEnd,
      nativeLanguage: user.nativeLanguage && ctx.i18n.t(`COMMAND_NATIVE_LANGUAGE_BUTTON_${user.nativeLanguage.toUpperCase()}`),
      language: undefined,
      level: undefined,
    }

    if (user.language) {
      const language = await this.languagesService.getLanguageById(user.language)
      messageValues.language = language.name
    }

    if (user.level) {
      const level = await this.levelService.getLevelById(user.level)
      messageValues.level = `${level.name} (${level.code})`
    }

    await ctx.replyWithHTML(
      ctx.i18n.t('MESSAGE_SETTINGS', messageValues),
    )
  }
}
