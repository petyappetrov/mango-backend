/**
 * Disabled command
 * Need enable when we add other languages
 */

import {
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common'
import { UsersService } from '../../users/users.service'
import { TelegramBotService } from '../telegram-bot.service'
import { Markup, ContextMessageUpdate } from 'telegraf'
import { Utils } from '../../utils'
import { LevelCommand } from './level.command'

@Injectable()
export class NativeLanguageCommand implements OnApplicationBootstrap {
  constructor(
    private readonly usersService: UsersService,
    private readonly botService: TelegramBotService,
    private readonly levelCommand: LevelCommand,
    private readonly utils: Utils,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command('native_language', this.command.bind(this))
    this.botService.bot.action(/nativeLanguage/, this.action.bind(this))
  }

  public command(ctx: ContextMessageUpdate) {
    ctx.reply(
      ctx.i18n.t('COMMAND_NATIVE_LANGUAGE_QUESTION'),
      Markup.inlineKeyboard(
        [
          Markup.callbackButton(ctx.i18n.t('COMMAND_NATIVE_LANGUAGE_BUTTON_RU'), 'nativeLanguage~ru'),
          Markup.callbackButton(ctx.i18n.t('COMMAND_NATIVE_LANGUAGE_BUTTON_EN'), 'nativeLanguage~en'),
        ],
      ).extra(),
    )
  }

  private async action(ctx: ContextMessageUpdate) {
    const nativeLanguage = ctx.callbackQuery.data.split('~')[1]
    const user = await this.usersService.setNativeLanguageUser(ctx.from.id, nativeLanguage)

    ctx.i18n.locale(nativeLanguage)

    ctx.answerCbQuery(ctx.i18n.t('COMMAND_NATIVE_LANGUAGE_NOTIFICATION'))
    ctx.editMessageText(
      ctx.i18n.t('COMMAND_NATIVE_LANGUAGE_ANSWER', { language: ctx.i18n.t(`COMMAND_NATIVE_LANGUAGE_BUTTON_${nativeLanguage.toUpperCase()}`) }),
      {
        parse_mode: 'HTML',
      },
    )

    if (this.usersService.userIsDisabled(user)) {
      await this.utils.wait(1000)
      this.levelCommand.levelCommand(ctx)
    }
  }
}
