import {
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common'
import { UsersService } from '../../users/users.service'
import { TelegramBotService } from '../telegram-bot.service'
import { Markup, ContextMessageUpdate } from 'telegraf'
import { Utils } from '../../utils'
import { Types } from 'mongoose'
import { LevelsService } from '../../levels/levels.service'
import { Level } from '../../levels/interfaces/level.interface'

@Injectable()
export class LevelCommand implements OnApplicationBootstrap {
  constructor(
    private readonly usersService: UsersService,
    private readonly botService: TelegramBotService,
    private readonly levelService: LevelsService,
    private readonly utils: Utils,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command('level', this.levelCommand.bind(this))
    this.botService.bot.action(/level/, this.levelAction.bind(this))
  }

  public async levelCommand(ctx: ContextMessageUpdate) {
    const levels: Level[] = await this.levelService.getLevels()
    const sortedLevels = this.utils.sortArrayByField(levels, 'code')
    ctx.reply(
      ctx.i18n.t('COMMAND_LEVEL_QUESTION_OF_ENGLISH'),
      Markup.inlineKeyboard(
        sortedLevels.map((level) =>
          Markup.callbackButton(`${level.name} (${level.code})`, `level~${level._id}`),
        ),
        { columns: 1 },
      ).extra(),
    )
  }

  private async levelAction(ctx: ContextMessageUpdate) {
    const levelId = ctx.callbackQuery.data.split('~')[1]
    const user = await this.usersService.setLevelUser(ctx.from.id, Types.ObjectId(levelId))
    const level = await this.levelService.getLevelById(Types.ObjectId(levelId))

    ctx.answerCbQuery(ctx.i18n.t('COMMAND_LEVEL_NOTIFICATION'))
    ctx.editMessageText(
      ctx.i18n.t('COMMAND_LEVEL_ANSWER', { level: `${level.name} (${level.code})` }),
      {
        parse_mode: 'HTML',
      },
    )

    if (this.usersService.userIsDisabled(user)) {
      await this.utils.wait(1000)
      ctx.scene.enter('timezone')
    }
  }
}
