import { Injectable, OnApplicationBootstrap, Inject, forwardRef } from '@nestjs/common'
import { Types } from 'mongoose'
import { Markup, ContextMessageUpdate } from 'telegraf'
import { InlineKeyboardMarkup, Message } from 'telegram-typings'
import { TelegramBotService } from '../telegram-bot.service'
import { UsersService } from '../../users/users.service'
import { Word } from '../../words/interfaces/word.interface'
import { CardsService } from '../../users/cards.service'
import { Utils } from '../../utils'
import { CardsRepeatCommand } from './cards-repeat.command'
import { User } from '../../users/interfaces/user.interface'

export const LIMIT_CARDS_PER_DAY = 7
export const CHOOSED_ICONS = ['✨', '⭐️', '💫', '☄️', '⚡️', '🌙', '🌓', '☀️', '🪐']
export const UNCHOOSED_ICONS = ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣', '🔟']

@Injectable()
export class CardsChooseCommand implements OnApplicationBootstrap {
  constructor(
    @Inject(forwardRef(() => TelegramBotService))
    private readonly botService: TelegramBotService,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly cardsService: CardsService,
    private readonly cardsRepeatCommand: CardsRepeatCommand,
    private readonly utils: Utils,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command(
      'add',
      this.botService.onlyDevelopers.bind(this.botService),
      (ctx: ContextMessageUpdate) => this.startChooseWordsCommand(ctx.session.user),
    )

    this.botService.bot.action(/cards_choose_know/, this.toKnowWordAction.bind(this))
    this.botService.bot.action(/cards_choose_learn/, this.toLearnWordAction.bind(this))
  }

  public async startChooseWordsCommand(user: User) {
    const word = await this.usersService.getUnlearnedRandomWordUser(user._id)
    const messageText = await this.getMessageText(user._id, word)
    const messageKeyboard = this.getMessageKeyboard(word)

    this.botService.bot.telegram.sendMessage(user.telegram, messageText, {
      reply_markup: messageKeyboard,
      parse_mode: 'HTML',
    })
  }

  public async nextChooseWordCommand(ctx: ContextMessageUpdate) {
    const word = await this.usersService.getUnlearnedRandomWordUser(ctx.session.user._id)
    const message = ((ctx.update || {}).callback_query || {}).message
    const messageText = await this.getMessageText(ctx.session.user._id, word, message)
    const messageKeyboard = this.getMessageKeyboard(word)

    ctx.editMessageText(
      messageText,
      {
        parse_mode: 'HTML',
        reply_markup: messageKeyboard,
      },
    )
  }

  private async toKnowWordAction(ctx: ContextMessageUpdate) {
    const wordId = ctx.callbackQuery.data.split('~')[1]
    const count = await this.cardsService.getCountNewCards(ctx.session.user._id)

    if (count >= LIMIT_CARDS_PER_DAY) {
      return ctx.answerCbQuery('Вы превысили лимит!')
    }

    await this.usersService.setKnowWordUser(ctx.from.id, Types.ObjectId(wordId))

    ctx.answerCbQuery('Я больше не буду отправлять это слово.')

    this.nextChooseWordCommand(ctx)
  }

  private async toLearnWordAction(ctx: ContextMessageUpdate) {
    const wordId = ctx.callbackQuery.data.split('~')[1]
    const count = await this.cardsService.getCountNewCards(ctx.session.user._id)

    if (count >= LIMIT_CARDS_PER_DAY) {
      return ctx.answerCbQuery('Вы превысили лимит!')
    }

    await this.cardsService.createCard({ user: ctx.session.user._id, word: Types.ObjectId(wordId) })

    ctx.answerCbQuery('Отлично! Будем повторять это слово.')

    if (count + 1 < LIMIT_CARDS_PER_DAY) {
      return this.nextChooseWordCommand(ctx)
    }

    this.cardsRepeatCommand.nextRepeatCardCommand(ctx)
  }

  private async getMessageText(userId: Types.ObjectId, word: Word, message?: Message): Promise<string> {
    const count = await this.cardsService.getCountNewCards(userId)
    const progress = await this.getMessageProgress(count, message)
    const example = this.getMessageExample(word)
    const wordText = `\n<b>${word.word}</b> — ${word.translate}\n${word.transcription} ${word.type}`

    return 'Выберите незнакомые слова:\n' + wordText + example + progress
  }

  private getMessageExample(word: Word): string {
    if (!word.examples || !word.examples.length) {
      return ''
    }

    const example = this.utils.getRandomArrayElement(word.examples)
    const examples = example
      .split('—')
      .filter(Boolean)
      .map((line) => `\n- ${line.trim()}`)
      .join('')

    return '\n<i>' + examples + '</i>'
  }

  private async getMessageProgress(count: number, message?: Message): Promise<string> {
    if (!count || !message) {
      return `\n\n<pre>${UNCHOOSED_ICONS.slice(0, LIMIT_CARDS_PER_DAY).join(' ')}</pre>`
    }

    const { entities, text } = message
    const entity = entities.find((m) => m.type === 'pre')
    const usedIcons = text.substr(entity.offset, entity.length).split(' ')

    const newIcons = usedIcons.map((icon, index) => {
      if (count - 1 === index) {
        return '🚀'
      }
      if (count - 2 === index) {
        return this.utils.getRandomArrayElement(CHOOSED_ICONS.filter((i) => !usedIcons.includes(i)))
      }
      return icon
    })
    return `\n\n<pre>${newIcons.join(' ')}</pre>`
  }

  private getMessageKeyboard(word: Word): Markup & InlineKeyboardMarkup {
    return Markup.inlineKeyboard([
      Markup.callbackButton('✅ Будем учить', `cards_choose_learn~${word._id}`),
      Markup.callbackButton('🤓 Я знаю это слово', `cards_choose_know~${word._id}`),
    ], { columns: 1 })
  }
}
