import {
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common'
import { ContextMessageUpdate } from 'telegraf'
import { TelegramBotService } from '../telegram-bot.service'

@Injectable()
export class HelpCommand implements OnApplicationBootstrap {
  constructor(
    private readonly botService: TelegramBotService,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command('help', this.helpCommand.bind(this))
  }

  public helpCommand(ctx: ContextMessageUpdate) {
    ctx.replyWithHTML(ctx.i18n.t('MESSAGE_HELP'))
  }
}
