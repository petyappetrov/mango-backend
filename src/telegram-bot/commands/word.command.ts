// TODO: Need remove this file

import { Injectable, OnApplicationBootstrap, Inject, forwardRef } from '@nestjs/common'
import { Markup } from 'telegraf'
import { Types } from 'mongoose'
import { UsersService } from '../../users/users.service'
import { TelegramBotService } from '../telegram-bot.service'
import { ContextMessageUpdate } from 'telegraf'
import { Utils } from '../../utils'
import { WordsService } from '../../words/words.service'
import { GoogleAPIService } from '../../google-api/google-api.service'
import { UsersSchedules } from '../../users/users.schedules'
import { User } from '../../users/interfaces/user.interface'
import { Word } from '../../words/interfaces/word.interface'
import { CardsService } from '../../users/cards.service'

@Injectable()
export class WordCommand implements OnApplicationBootstrap {
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    @Inject(forwardRef(() => TelegramBotService))
    private readonly botService: TelegramBotService,
    @Inject(forwardRef(() => UsersSchedules))
    private readonly usersSchedules: UsersSchedules,
    private readonly wordsService: WordsService,
    private readonly googleAPIService: GoogleAPIService,
    private readonly cardsService: CardsService,
    private readonly utils: Utils,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command(
      'word',
      this.botService.onlyDevelopers.bind(this),
      this.commandSend.bind(this),
    )
    this.botService.bot.action(/example/, this.actionExample.bind(this))
    this.botService.bot.action(/voice/, this.actionVoice.bind(this))
    this.botService.bot.action(/remember/, this.actionRemember.bind(this))
  }

  private async commandSend(ctx: ContextMessageUpdate) {
    const user = await this.usersService.getUserByTelegram(ctx.from.id)
    const cards = await this.cardsService.getCardsOfToday(user._id)
    this.sendWordToUser(user, cards[0].word, true)
  }

  private async actionExample(ctx: ContextMessageUpdate) {
    ctx.answerCbQuery()
    const wordId = ctx.callbackQuery.data.split('~')[1]
    const word = await this.wordsService.getWordById(Types.ObjectId(wordId))

    const example = this.utils.getRandomArrayElement(word.examples)
    const examples = example
      .split('—')
      .filter(Boolean)
      .map((line) => `\n- ${line.trim()}`)
      .join('')

    const message = this.transformWordToHtml(word) + '\n<i>' + examples + '</i>'

    ctx.editMessageText(message, {
      parse_mode: 'HTML',
      reply_markup: {
        inline_keyboard: this.utils.telegramRemoveKeyboard(
          ctx.update.callback_query.message,
          'example',
        ),
      },
    })
  }

  private async actionVoice(ctx: ContextMessageUpdate) {
    ctx.answerCbQuery()
    const wordId = ctx.callbackQuery.data.split('~')[1]
    const word = await this.wordsService.getWordById(Types.ObjectId(wordId))

    await ctx.editMessageReplyMarkup({
      inline_keyboard: this.utils.telegramRemoveKeyboard(
        ctx.update.callback_query.message,
        'voice',
      ),
    })

    if (word.voice) {
      return ctx.replyWithVoice(word.voice, {
        reply_to_message_id: ctx.update.callback_query.message.message_id,
      }).catch((error) => {
        console.log(error.message)
        return this.generateNewVoice(ctx, word)
      })
    }

    this.generateNewVoice(ctx, word)
  }

  private async generateNewVoice(ctx: ContextMessageUpdate, word: Word) {
    const source = await this.googleAPIService.synthesizeVoiceFromText(word.word)
    const audioMessage = await ctx.replyWithVoice(
      { source },
      {
        reply_to_message_id: ctx.update.callback_query.message.message_id,
      },
    )

    this.wordsService.saveVoice(word._id, audioMessage.voice.file_id)
  }

  private async actionRemember(ctx: ContextMessageUpdate) {
    const wordId = ctx.callbackQuery.data.split('~')[1]
    // await this.usersService.setRememberWordUser(ctx.from.id, Types.ObjectId(wordId))
    await ctx.editMessageReplyMarkup({
      inline_keyboard: this.utils.telegramRemoveKeyboard(
        ctx.update.callback_query.message,
        'remember',
      ),
    })
    await ctx.answerCbQuery(ctx.i18n.t('UNREAD_WORD_BUTTON_REMEMBER_ANSWER'))
  }

  public async sendWordToUser(user: User, wordId: Types.ObjectId, withRememberButton?: boolean) {
    const word = await this.wordsService.getWordById(wordId)
    const buttons = this.getWordButtons(word, withRememberButton)
    await this.botService.bot.telegram.sendMessage(
      user.telegram,
      this.transformWordToHtml(word),
      {
        reply_markup: Markup.inlineKeyboard(buttons, { columns: 2 }),
        parse_mode: 'HTML',
      },
    ).catch(async (error) => {
      if (error.response.error_code === 403) {
        await this.usersService.disableUser(user._id)
        this.usersSchedules.stopSchedule(user)
      }
    })
  }

  public transformWordToHtml(word: Word) {
    return `<b>${word.word}</b> — ${word.translate}\n${word.transcription} ${word.type}`
  }

  private getWordButtons(word: Word, withRememberButton: boolean) {
    const buttons = [
      Markup.callbackButton(
        this.getTranslateText('ru', 'UNREAD_WORD_BUTTON_VOICE'),
        `voice~${word._id}`,
      ),
    ]

    if (word.examples && word.examples.length) {
      buttons.push(
        Markup.callbackButton(
          this.getTranslateText('ru', 'UNREAD_WORD_BUTTON_EXAMPLE'),
          `example~${word._id}`,
        ),
      )
    }

    if (withRememberButton) {
      buttons.push(
        Markup.callbackButton(
          this.getTranslateText('ru', 'UNREAD_WORD_BUTTON_REMEMBER'),
          `remember~${word._id}`,
        ),
      )
    }

    return buttons
  }

  private getTranslateText(language: string, path: string, values?: object) {
    const repository = this.botService.i18n.repository[language]
    if (repository && typeof repository[path] === 'function') {
      return repository[path](values)
    }
    return path
  }
}
