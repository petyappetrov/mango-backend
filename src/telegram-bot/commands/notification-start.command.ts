import { Injectable, OnApplicationBootstrap } from '@nestjs/common'
import { UsersService } from '../../users/users.service'
import { TelegramBotService } from '../telegram-bot.service'
import { Markup, ContextMessageUpdate } from 'telegraf'
import { Utils } from '../../utils'
import { NotificationEndCommand } from './notification-end.command'
import { UsersSchedules } from '../../users/users.schedules'

@Injectable()
export class NotificationStartCommand implements OnApplicationBootstrap {
  constructor(
    private readonly usersService: UsersService,
    private readonly usersSchedules: UsersSchedules,
    private readonly botService: TelegramBotService,
    private readonly notificationEndCommand: NotificationEndCommand,
    private readonly utils: Utils,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command('notifications_start', this.command.bind(this))
    this.botService.bot.action(/notificationsStart/, this.action.bind(this))
  }

  public async command(ctx: ContextMessageUpdate) {
    ctx.reply(
      ctx.i18n.t('COMMAND_NOTIFICATION_START_QUESTION'),
      Markup.inlineKeyboard(
        ['06:00', '07:00', '08:00', '09:00', '10:00', '11:00'].map((value) =>
          Markup.callbackButton(`${value}`, `notificationsStart~${value}`),
        ),
        { columns: 3 },
      ).extra(),
    )
  }

  private async action(ctx: ContextMessageUpdate) {
    const time = ctx.callbackQuery.data.split('~')[1]
    const user = await this.usersService.setNotificationsStartUser(ctx.from.id, time)

    ctx.answerCbQuery(ctx.i18n.t('COMMAND_NOTIFICATION_START_NOTIFICATION'))
    ctx.editMessageText(
      ctx.i18n.t('COMMAND_NOTIFICATION_START_ANSWER', { time }),
      {
        parse_mode: 'HTML',
      },
    )

    this.usersSchedules.startSchedule(user)

    if (this.usersService.userIsDisabled(user)) {
      await this.utils.wait(1000)
      this.notificationEndCommand.command(ctx)
    }
  }
}
