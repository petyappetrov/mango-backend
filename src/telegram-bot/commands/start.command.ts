import {
  Injectable,
  OnApplicationBootstrap,
} from '@nestjs/common'
import { ContextMessageUpdate } from 'telegraf'
import { UsersService } from '../../users/users.service'
import { TelegramBotService } from '../telegram-bot.service'
import { Utils } from '../../utils'
import { UsersSchedules } from '../../users/users.schedules'
import { LanguagesService } from '../../languages/languages.service'
import { LevelCommand } from './level.command'
import { HelpCommand } from './help.command'

@Injectable()
export class StartCommand implements OnApplicationBootstrap {
  constructor(
    private readonly usersService: UsersService,
    private readonly languagesService: LanguagesService,
    private readonly usersSchedules: UsersSchedules,
    private readonly botService: TelegramBotService,
    private readonly levelCommand: LevelCommand,
    private readonly helpCommand: HelpCommand,
    private readonly utils: Utils,
  ) {}

  onApplicationBootstrap() {
    this.botService.bot.command('start', this.command.bind(this))
  }

  async command(ctx: ContextMessageUpdate) {
    if (ctx.session.user) {
      if (
        !ctx.session.user.notificationsStart ||
        !ctx.session.user.notificationsEnd ||
        !ctx.session.user.timezone ||
        !ctx.session.user.level
      ) {
        ctx.replyWithHTML(ctx.i18n.t('COMMAND_START_WELCOME_BACK_DO_NOT_REGISTERED'))
        return this.helpCommand.helpCommand(ctx)
      }

      if (this.usersService.userIsDisabled(ctx.session.user)) {
        await this.usersService.enableUser(ctx.session.user._id)
        this.usersSchedules.startSchedule(ctx.session.user)
      }
      return ctx.replyWithHTML(ctx.i18n.t('COMMAND_START_WELCOME_BACK'))
    }

    ctx.session.language = ctx.from.language_code

    /**
     * Hardcoded because we have only single language
     * Need delete when we add other languages
     */
    ctx.i18n.locale('ru')

    await ctx.replyWithHTML(ctx.i18n.t('COMMAND_START_MESSAGE'))

    const englishLanguage = await this.languagesService.getLanguageByCode('en')
    await this.usersService.createUser({
      username: ctx.from.username,
      firstName: ctx.from.first_name,
      telegram: ctx.from.id,
      nativeLanguage: ctx.from.language_code,

      /**
       * Hardcoded because we have only single language
       * Need delete when we add other languages
       */
      language: englishLanguage._id,
    })

    await this.utils.wait(2000)

    this.levelCommand.levelCommand(ctx)
  }
}
