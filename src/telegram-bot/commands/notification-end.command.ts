import { Injectable, OnApplicationBootstrap } from '@nestjs/common'
import { UsersService } from '../../users/users.service'
import { TelegramBotService } from '../telegram-bot.service'
import { Markup, ContextMessageUpdate, Message } from 'telegraf'
import { Utils } from '../../utils'
import { UsersSchedules } from '../../users/users.schedules'
import { SettingsCommand } from './settings.command'
import { ConfigService } from '../../config.service'
import { CardsChooseCommand } from './cards-choose.command'

@Injectable()
export class NotificationEndCommand implements OnApplicationBootstrap {
  constructor(
    private readonly usersService: UsersService,
    private readonly usersSchedules: UsersSchedules,
    private readonly botService: TelegramBotService,
    private readonly utils: Utils,
    private readonly settingsCommand: SettingsCommand,
    private readonly cardsChooseCommand: CardsChooseCommand,
    private readonly configService: ConfigService,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command('notifications_end', this.command.bind(this))
    this.botService.bot.action(/notificationsEnd/, this.action.bind(this))
  }

  public async command(ctx: ContextMessageUpdate) {
    ctx.reply(
      ctx.i18n.t('COMMAND_NOTIFICATION_END_QUESTION'),
      Markup.inlineKeyboard(
        ['18:00', '19:00', '20:00', '21:00', '22:00', '23:00'].map((value) =>
          Markup.callbackButton(`${value}`, `notificationsEnd~${value}`),
        ),
        { columns: 3 },
      ).extra(),
    )
  }

  private async action(ctx: ContextMessageUpdate) {
    const time = ctx.callbackQuery.data.split('~')[1]
    const user = await this.usersService.setNotificationsEndUser(
      ctx.from.id,
      time,
    )

    ctx.answerCbQuery(ctx.i18n.t('COMMAND_NOTIFICATION_END_NOTIFICATION'))
    ctx.editMessageText(
      ctx.i18n.t('COMMAND_NOTIFICATION_END_ANSWER', { time }),
      {
        parse_mode: 'HTML',
      },
    )

    this.usersSchedules.startSchedule(user)

    if (this.usersService.userIsDisabled(user) && !user.knowsWords.length) {
      await this.usersService.enableUser(user._id)
      await this.settingsCommand.command(ctx)
      await this.utils.wait(2000)
      await ctx.reply(ctx.i18n.t('MESSAGE_FINISH_SUCCESS'), { disable_notification: true })
      await this.utils.wait(2000)
      await ctx.reply(ctx.i18n.t('MESSAGE_FINISH_LAUNCH'), { disable_notification: true })
      await this.utils.wait(2000)
      await ctx.reply(ctx.i18n.t('MESSAGE_FINISH_FIRST_MESSAGE'), { disable_notification: true })
      await this.utils.wait(2000)
      await this.cardsChooseCommand.startChooseWordsCommand(user)

      /**
       * Send message to developer
       */
      const count = await this.usersService.getCountUsers()
      const name = user.username ? '@' + user.username : user.firstName
      this.botService.bot.telegram.sendMessage(
        this.configService.get('DEVELOPER_TELEGRAM_ID'),
        'You have a new user: ' + name + '\nCount users: ' + count,
      )
    }
  }
}
