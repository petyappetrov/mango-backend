import { Injectable, OnApplicationBootstrap, Inject, forwardRef } from '@nestjs/common'
import { Types } from 'mongoose'
import { Markup, ContextMessageUpdate } from 'telegraf'
import { InlineKeyboardMarkup, Message } from 'telegram-typings'
import { TelegramBotService } from '../telegram-bot.service'
import { CardsService } from '../../users/cards.service'
import { WordsService } from '../../words/words.service'
import { Card } from '../../users/interfaces/card.interface'
import { Word } from '../../words/interfaces/word.interface'

@Injectable()
export class CardsRepeatCommand implements OnApplicationBootstrap {
  constructor(
    @Inject(forwardRef(() => TelegramBotService))
    private readonly botService: TelegramBotService,
    private readonly wordsService: WordsService,
    private readonly cardsService: CardsService,
  ) {}

  async onApplicationBootstrap() {
    this.botService.bot.command(
      'repeat',
      this.botService.onlyDevelopers.bind(this.botService),
      this.startRepeatCardsCommand.bind(this),
    )

    this.botService.bot.action(/repeat_show/, this.toShowTranslateAction.bind(this))
    this.botService.bot.action(/repeat_answer_forget/, this.toAnswerForgetAction.bind(this))
    this.botService.bot.action(/repeat_answer_hard/, this.toAnswerHardAction.bind(this))
    this.botService.bot.action(/repeat_answer_easy/, this.toAnswerEasyAction.bind(this))
  }

  public async startRepeatCardsCommand(ctx: ContextMessageUpdate) {
    const cards = await this.cardsService.getCardsOfToday(ctx.session.user._id)
    const count = cards.length

    if (!count) {
      return ctx.reply('Слова были успешно изучены и повторены. 🧑🏻‍🚀')
    }

    const word = await this.wordsService.getWordById(cards[0].word)
    const progress = this.getMessageProgress(count)
    const messageText = this.getMessageText(word, true)

    ctx.reply(
      'Постарайтесь вспомнить слово:\n' +
      messageText +
      progress,
      {
        parse_mode: 'HTML',
        reply_markup: this.getTranslateKeyboard(cards[0]),
      },
    )
  }

  public async nextRepeatCardCommand(ctx: ContextMessageUpdate) {
    const cards = await this.cardsService.getCardsOfToday(ctx.session.user._id)
    const count = cards.length

    if (!count) {
      return ctx.editMessageText('Слова были успешно изучены и повторены. 🧑🏻‍🚀')
    }

    const word = await this.wordsService.getWordById(cards[0].word)
    const progress = this.getMessageProgress(count)
    const messageText = this.getMessageText(word, true)

    ctx.editMessageText(
      'Постарайтесь вспомнить слово:\n' +
      messageText +
      progress,
      {
        parse_mode: 'HTML',
        reply_markup: this.getTranslateKeyboard(cards[0]),
      },
    )
  }

  private async toShowTranslateAction(ctx: ContextMessageUpdate) {
    ctx.answerCbQuery()
    const cardId = ctx.callbackQuery.data.split('~')[1]
    const card = await this.cardsService.getCardById(Types.ObjectId(cardId))
    const count = await this.cardsService.getCountNewCards(ctx.session.user._id)
    const word = await this.wordsService.getWordById(card.word)
    const messageText = this.getMessageText(word)
    const progress = this.getMessageProgress(count)
    ctx.editMessageText(
      'Получилось вспомнить слово?\n' +
      messageText +
      progress,
      {
        parse_mode: 'HTML',
        reply_markup: this.getAnswerKeyboard(card),
      },
    )
  }

  private async toAnswerForgetAction(ctx: ContextMessageUpdate) {
    ctx.answerCbQuery()
    const cardId = ctx.callbackQuery.data.split('~')[1]

    await this.cardsService.forgetCard(Types.ObjectId(cardId))

    this.nextRepeatCardCommand(ctx)
  }

  private async toAnswerHardAction(ctx: ContextMessageUpdate) {
    ctx.answerCbQuery()
    const cardId = ctx.callbackQuery.data.split('~')[1]

    await this.cardsService.setNextLevelLearningDateCard(Types.ObjectId(cardId), 0)

    this.nextRepeatCardCommand(ctx)
  }

  private async toAnswerEasyAction(ctx: ContextMessageUpdate) {
    ctx.answerCbQuery()
    const cardId = ctx.callbackQuery.data.split('~')[1]

    await this.cardsService.setNextLevelLearningDateCard(Types.ObjectId(cardId), 2)

    this.nextRepeatCardCommand(ctx)
  }

  private getTranslateKeyboard(card: Card): Markup & InlineKeyboardMarkup {
    return Markup.inlineKeyboard([
      Markup.callbackButton('Показать перевод', `repeat_show~${card._id}`),
    ], { columns: 1 })
  }

  private getAnswerKeyboard(card: Card): Markup & InlineKeyboardMarkup {
    return Markup.inlineKeyboard([
      Markup.callbackButton('👽 Забыл', `repeat_answer_forget~${card._id}`),
      Markup.callbackButton('🤯 Трудно', `repeat_answer_hard~${card._id}`),
      Markup.callbackButton('😎 Легко', `repeat_answer_easy~${card._id}`),
    ])
  }

  private getMessageText(word: Word, hideTranslate?: boolean): string {
    return `\n<b>${word.word}</b> — ${hideTranslate ? '?' : word.translate}\n${word.transcription} ${word.type}`
  }

  private getMessageProgress(count: number): string {
    return `\n\nОсталось слов: ${count}`
  }
}
