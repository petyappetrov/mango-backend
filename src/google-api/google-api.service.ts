import { Injectable, OnModuleInit } from '@nestjs/common'
import * as googleMapsClient from '@google/maps'
import textToSpeech from '@google-cloud/text-to-speech'
import { ConfigService } from '../config.service'

@Injectable()
export class GoogleAPIService implements OnModuleInit {
  private maps: googleMapsClient.GoogleMapsClient

  constructor(private readonly configService: ConfigService) {}

  onModuleInit() {
    const options = {
      key: this.configService.get('GOOGLE_MAPS_API_KEY'),
      Promise,
    }
    this.maps = googleMapsClient.createClient(options)
  }

  public async getTimezoneFromAddress(address: string): Promise<googleMapsClient.ClientResponse<googleMapsClient.TimeZoneResponse>> {
    const geocode = await this.maps.geocode({ address }).asPromise()

    if (geocode.json.status !== 'OK') {
      throw Error(geocode.json.status)
    }

    const { lat, lng } = geocode.json.results[0].geometry.location

    return this.maps.timezone({ location: lat + ',' + lng }).asPromise()
  }

  public async synthesizeVoiceFromText(text: string): Promise<Buffer> {
    const client = new textToSpeech.TextToSpeechClient()

    const [result] = await client.synthesizeSpeech({
      input: { text },
      voice: { languageCode: 'en-EN', ssmlGender: 3 },
      audioConfig: { audioEncoding: 2 },
    })

    return Buffer.from(result.audioContent)
  }
}
