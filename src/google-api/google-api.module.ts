import { Module } from '@nestjs/common'
import { GoogleAPIService } from './google-api.service'
import { ConfigModule } from '../config.module'

@Module({
  imports: [ConfigModule],
  providers: [GoogleAPIService],
  exports: [GoogleAPIService],
})
export class GoogleAPIModule {}
