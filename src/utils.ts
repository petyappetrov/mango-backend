import { Injectable } from '@nestjs/common'
import { Message } from 'telegraf'

@Injectable()
export class Utils {
  wait(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms))
  }

  sortArrayByField<T>(array: T[], field: string): T[] {
    return array.sort((a: any, b: any) => {
      if (a[field] > b[field]) {
        return 1
      }
      if (a[field] < b[field]) {
        return -1
      }
      return 0
    })
  }

  getRandomArrayElement<T>(array: T[]): T {
    return array[Math.floor(Math.random() * array.length)]
  }

  shuffleArray<T>(array: T[]): T[] {
    const a = [...array]
    return a.sort(() => Math.random() - 0.5)
  }

  telegramRemoveKeyboard(message: Message, exclude: string) {
    if (!message.reply_markup) {
      return []
    }

    return message.reply_markup.inline_keyboard.map((keyboard: any) =>
      keyboard.filter(
        (button: any) => !button.callback_data.includes(exclude),
      ),
    )
  }
}
