import { Controller, Get, Response, Post, Body, Delete, Param, UseGuards } from '@nestjs/common'
import { Response as ResponseType } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { LevelsService } from './levels.service'

@UseGuards(AuthGuard('jwt'))
@Controller('levels')
export class LevelsController {
  constructor(private levelsService: LevelsService) {}

  @Get()
  public async getLevels(
    @Response() response: ResponseType,
  ) {
    const levels = await this.levelsService.getLevels()
    return response.status(200).json(levels)
  }
}
