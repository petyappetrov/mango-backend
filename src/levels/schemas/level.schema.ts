import * as mongoose from 'mongoose'

export const LevelSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  },
}, {
  versionKey: false,
})
