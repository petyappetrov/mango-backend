import { Document } from 'mongoose'

export interface Level extends Document {
  readonly _id: string
  readonly name: string
  readonly code: string
}
