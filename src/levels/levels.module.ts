import { MongooseModule } from '@nestjs/mongoose'
import { Module } from '@nestjs/common'
import { LevelsService } from './levels.service'
import { LevelsController } from './levels.controllers'
import { LevelSchema } from './schemas/level.schema'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Level', schema: LevelSchema }]),
  ],
  controllers: [LevelsController],
  providers: [LevelsService],
  exports: [LevelsService],
})
export class LevelsModule {}
