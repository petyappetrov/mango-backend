import { Model, Types } from 'mongoose'
import { Injectable, OnModuleInit } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Level } from './interfaces/level.interface'
import LevelFixtures from './schemas/level.fixtures.json'

@Injectable()
export class LevelsService implements OnModuleInit {
  constructor(@InjectModel('Level') private readonly levelModel: Model<Level>) {}

  async onModuleInit() {
    const levels = await this.getLevels()
    if (!levels.length) {
      this.levelModel.create(LevelFixtures)
    }
  }

  public async getLevels(): Promise<Level[]> {
    return await this.levelModel.find({})
  }

  public async getLevelByCode(code: string): Promise<Level> {
    return await this.levelModel.findOne({ code })
  }

  public async getLevelById(id: Types.ObjectId): Promise<Level> {
    return await this.levelModel.findById(id)
  }
}
