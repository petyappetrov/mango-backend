import { IsNotEmpty } from 'class-validator'

export class CreateLanguageDTO {
  @IsNotEmpty()
  readonly name: string

  @IsNotEmpty()
  readonly code: string
}
