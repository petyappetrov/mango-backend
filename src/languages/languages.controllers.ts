import { Controller, Get, Response, Post, Body, Delete, Param, UseGuards } from '@nestjs/common'
import { Response as ResponseType } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { LanguagesService } from './languages.service'
import { CreateLanguageDTO } from './dto/create-language.dto'

@UseGuards(AuthGuard('jwt'))
@Controller('languages')
export class LanguagesController {
  constructor(private languagesService: LanguagesService) {}

  @Post('create')
  public async createLanguage(
    @Response() response: ResponseType,
    @Body() createLanguageDTO: CreateLanguageDTO,
  ) {
    const language = await this.languagesService.createLanguage(createLanguageDTO)
    return response.status(200).json(language)
  }

  @Get()
  public async getLanguages(
    @Response() response: ResponseType,
  ) {
    const languages = await this.languagesService.getLanguages()
    return response.status(200).json(languages)
  }

  @Delete()
  public async removeLanguage(
    @Response() response: ResponseType,
    @Param('id') id: string,
  ) {
    await this.languagesService.removeLanguage(id)
    return response.status(200)
  }
}
