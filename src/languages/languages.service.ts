import { Model, Types } from 'mongoose'
import { Injectable, OnModuleInit } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Language } from './interfaces/language.interface'
import { CreateLanguageDTO } from './dto/create-language.dto'
import LanguageFixtures from './schemas/language.fixtures.json'

@Injectable()
export class LanguagesService implements OnModuleInit {
  constructor(@InjectModel('Language') private readonly languageModel: Model<Language>) {}

  async onModuleInit() {
    const languages = await this.getLanguages()
    if (!languages.length) {
      this.languageModel.create(LanguageFixtures)
    }
  }

  public async createLanguage(createLanguageDTO: CreateLanguageDTO): Promise<Language> {
    return await this.languageModel.create(createLanguageDTO)
  }

  public async getLanguages(): Promise<Language[]> {
    return await this.languageModel.find({})
  }

  public async getLanguageById(id: Types.ObjectId): Promise<Language> {
    return await this.languageModel.findById(id)
  }

  public async getLanguageByCode(code: string): Promise<Language> {
    return await this.languageModel.findOne({ code })
  }

  public async removeLanguage(id: string): Promise<Language> {
    return await this.languageModel.findByIdAndRemove(id)
  }
}
