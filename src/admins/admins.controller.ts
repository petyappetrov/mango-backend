import {
  Controller,
  Get,
  Response,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common'
import { Response as ResponseType } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { AdminsService } from './admins.service'

@UseGuards(AuthGuard('jwt'))
@Controller('admins')
export class AdminsController {
  constructor(private adminsService: AdminsService) {}

  @Get()
  public async getAdmins(@Response() response: ResponseType) {
    const admins = await this.adminsService.getAdmins()
    return response.status(200).json(admins)
  }

  @Delete(':id')
  public async removeAdmin(
    @Response() response: ResponseType,
    @Param('id') id: string,
  ) {
    await this.adminsService.removeAdmin(id)
    return response.status(200)
  }
}
