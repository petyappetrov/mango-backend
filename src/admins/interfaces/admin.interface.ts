import { Document } from 'mongoose'

export interface Admin extends Document {
  readonly _id: string
  readonly name: string
  readonly email: string
  readonly password: string
}
