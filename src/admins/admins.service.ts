import { Model } from 'mongoose'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Admin } from './interfaces/admin.interface'
import { CreateAdminDTO } from './dto/create-admin.dto'

@Injectable()
export class AdminsService {
  constructor(@InjectModel('Admin') private readonly adminModel: Model<Admin>) {}

  public async createAdmin(createAdminDTO: CreateAdminDTO): Promise<Admin> {
    const admin = await this.adminModel.create(createAdminDTO)
    return admin
  }

  public async getAdminByEmail(email: string): Promise<Admin> {
    const admin = await this.adminModel.findOne({ email }).select('+password')
    return admin
  }

  public async getAdmins(): Promise<Admin[]> {
    return await this.adminModel.find({})
  }

  public async removeAdmin(id: string): Promise<Admin> {
    return await this.adminModel.findByIdAndRemove(id).exec()
  }
}
