import * as bcrypt from 'bcrypt'
import { Injectable, UnauthorizedException, OnModuleInit } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { AdminsService } from '../admins/admins.service'
import { Admin } from '../admins/interfaces/admin.interface'
import { CreateAdminDTO } from '../admins/dto/create-admin.dto'
import { ConfigService } from '../config.service'

@Injectable()
export class AuthService implements OnModuleInit {
  constructor(
    private readonly adminsService: AdminsService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async onModuleInit() {
    const admins = await this.adminsService.getAdmins()
    if (!admins.length) {
      this.register({
        name: this.configService.get('ADMIN_NAME'),
        email: this.configService.get('ADMIN_EMAIL'),
        password: this.configService.get('ADMIN_PASSWORD'),
      })
    }
  }

  async validateAdmin(email: string, password: string): Promise<Admin | boolean> {
    const admin = await this.adminsService.getAdminByEmail(email)
    if (!admin) {
      throw new UnauthorizedException({
        email: 'Администратор с таким email не найден',
      })
    }
    const isMatch = await bcrypt.compare(password, admin.password)
    if (!isMatch) {
      throw new UnauthorizedException({
        password: 'Неверный пароль',
      })
    }
    return admin
  }

  async login(admin: Admin) {
    const payload = { _id: admin._id, email: admin.email }
    return {
      token: this.jwtService.sign(payload),
    }
  }

  public async register(createAdminDTO: CreateAdminDTO): Promise<{ token: string }> {
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(createAdminDTO.password, salt)
    const admin = await this.adminsService.createAdmin({ ...createAdminDTO, password: hash })
    return {
      token: this.jwtService.sign({ _id: admin._id, email: admin.email }),
    }
  }
}
