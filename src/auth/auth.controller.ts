import { Controller, Body, Post, UseGuards, Get, Request, Response } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { Request as RequestType, Response as ResponseType } from 'express'
import { AuthService } from './auth.service'
import { CreateAdminDTO } from '../admins/dto/create-admin.dto'

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req: any): Promise<{ token: string }> {
    return this.authService.login(req.user)
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  getProfile(@Request() req: RequestType) {
    return req.user
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('register')
  public async register(
    @Response() response: ResponseType,
    @Body() createAdminDTO: CreateAdminDTO,
  ) {
    const admin = await this.authService.register(createAdminDTO)
    return response.status(200).json(admin)
  }
}
