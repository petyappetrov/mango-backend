import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async () => {
        const mongod = new MongoMemoryServer();
        const uri = await mongod.getConnectionString()
        return {
          uri,
          useFindAndModify: false,
          useUnifiedTopology: true,
          useNewUrlParser: true,
        }
      },
    }),
  ],
})
export class DatabaseTestModule {}
