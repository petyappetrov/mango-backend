import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { AdminsModule } from './admins/admins.module'
import { AuthModule } from './auth/auth.module'
import { TelegramBotModule } from './telegram-bot/telegram-bot.module'
import { WordsModule } from './words/words.module'
import { UsersModule } from './users/users.module'
import { LanguagesModule } from './languages/languages.module'
import { LevelsModule } from './levels/levels.module'
import { GoogleAPIModule } from './google-api/google-api.module'
import { ConfigModule } from './config.module'
import { ConfigService } from './config.service'

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const host = configService.get('DB_HOST')
        const port = configService.get('DB_PORT')
        const name = configService.get('DB_NAME')
        const username = configService.get('DB_USERNAME')
        const password = configService.get('DB_PASSWORD')

        return {
          uri: `mongodb://${username}:${password}@${host}:${port}/${name}`,
          useCreateIndex: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          useNewUrlParser: true,
        }
      },
    }),
    AdminsModule,
    AuthModule,
    TelegramBotModule,
    WordsModule,
    UsersModule,
    LanguagesModule,
    LevelsModule,
    GoogleAPIModule,
    ConfigModule,
  ],
})
export class AppModule {}
