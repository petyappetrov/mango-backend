import { Controller, Get, Response, UseGuards, Delete, Param, Body } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { UsersService } from './users.service'
import { Response as ResponseType } from 'express'
import { GetUsersDTO } from './dto/get-users.dto'
import { Types } from 'mongoose'

@UseGuards(AuthGuard('jwt'))
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  async getUsers(
    @Response() response: ResponseType,
    @Body() getUsersDTO: GetUsersDTO = {},
  ) {
    const users = await this.usersService.getUsers(getUsersDTO)
    return response.status(200).json(users)
  }

  @Get('count')
  async getCountUsers(
    @Response() response: ResponseType,
  ) {
    const count = await this.usersService.getCountUsers()
    return response.status(200).json(count)
  }

  @Delete(':id')
  async removeUser(
    @Response() response: ResponseType,
    @Param('id') id: string,
  ) {
    const user = await this.usersService.removeUser(id)
    return response.status(200).json(user)
  }

  @Delete()
  async removeUsers(
    @Response() response: ResponseType,
    @Body('ids') ids: Types.ObjectId[],
  ) {
    const users = await this.usersService.removeUsers(ids)
    return response.status(200).json(users)
  }
}
