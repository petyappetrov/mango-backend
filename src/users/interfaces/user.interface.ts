import { Document, Types } from 'mongoose'

export interface User extends Document {
  readonly username?: string
  readonly firstName: string
  readonly telegram: number
  readonly language?: Types.ObjectId
  readonly nativeLanguage?: 'en' | 'ru'
  readonly level?: Types.ObjectId
  readonly notificationsStart?: string
  readonly notificationsEnd?: string
  readonly timezone?: string
  readonly interval: number
  readonly status: 'enabled' | 'disabled'
  readonly knowsWords?: Types.ObjectId[]
}
