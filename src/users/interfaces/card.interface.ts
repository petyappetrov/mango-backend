import { Document, Types } from 'mongoose'

export interface Card extends Document {
  readonly user: Types.ObjectId
  readonly word: Types.ObjectId
  readonly nextReviewDate: Date
  readonly intervalIndex: number
  readonly isFinish: boolean
}
