import { MongooseModule } from '@nestjs/mongoose'
import { Module, forwardRef } from '@nestjs/common'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'
import { UserSchema } from './schemas/user.schema'
import { WordsModule } from '../words/words.module'
import { UsersSchedules } from './users.schedules'
import { TelegramBotModule } from '../telegram-bot/telegram-bot.module'
import { CardSchema } from './schemas/card.schema'
import { CardsService } from './cards.service'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Card', schema: CardSchema }]),
    WordsModule,
    forwardRef(() => TelegramBotModule),
  ],
  controllers: [UsersController],
  providers: [
    UsersService,
    UsersSchedules,
    CardsService,
  ],
  exports: [
    UsersService,
    UsersSchedules,
    CardsService,
  ],
})
export class UsersModule {}
