import { Model, Types } from 'mongoose'
import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { User } from './interfaces/user.interface'
import { CreateUserDTO } from './dto/create-user.dto'
import { GetUsersDTO } from './dto/get-users.dto'
import { UsersSchedules } from './users.schedules'
import { Card } from './interfaces/card.interface'
import { WordsService } from '../words/words.service'
import { Word } from '../words/interfaces/word.interface'

interface MatchType {
  username?: object
  language?: object
}

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
    @InjectModel('Card') private readonly cardModel: Model<Card>,
    private readonly usersSchedules: UsersSchedules,
    private readonly wordsService: WordsService,
  ) {}

  public async createUser(createUserDTO: CreateUserDTO): Promise<User> {
    return await this.userModel.create(createUserDTO)
  }

  public async removeUser(id: string): Promise<User> {
    const user = await this.userModel.findByIdAndRemove(id).exec()
    this.usersSchedules.stopSchedule(user)
    return user
  }

  public async removeUsers(ids: Types.ObjectId[]): Promise<User[]> {
    const users = await Promise.all(ids.map((id) => this.userModel.findByIdAndRemove(id).exec()))
    users.forEach((user) => this.usersSchedules.stopSchedule(user))
    return users
  }

  public async findOneByTelegram(telegram: number): Promise<User> {
    return await this.userModel.findOne({ telegram })
  }

  public async setLanguageUser(
    telegram: number,
    language: Types.ObjectId | string,
  ): Promise<User> {
    return await this.userModel
      .findOneAndUpdate({ telegram }, { language }, { new: true })
      .exec()
  }

  public async setNativeLanguageUser(
    telegram: number,
    nativeLanguage: string,
  ): Promise<User> {
    return await this.userModel
      .findOneAndUpdate({ telegram }, { nativeLanguage }, { new: true })
      .exec()
  }

  public async setLevelUser(telegram: number, level: Types.ObjectId): Promise<User> {
    return await this.userModel
      .findOneAndUpdate({ telegram }, { level }, { new: true })
      .exec()
  }

  public async setNotificationsStartUser(
    telegram: number,
    notificationsStart: string,
  ): Promise<User> {
    return await this.userModel
      .findOneAndUpdate({ telegram }, { notificationsStart }, { new: true })
      .exec()
  }

  public async setNotificationsEndUser(
    telegram: number,
    notificationsEnd: string,
  ): Promise<User> {
    return await this.userModel
      .findOneAndUpdate({ telegram }, { notificationsEnd }, { new: true })
      .exec()
  }

  public async setTimezoneUser(
    telegram: number,
    timezone: string,
  ): Promise<User> {
    return await this.userModel
      .findOneAndUpdate({ telegram }, { timezone }, { new: true })
      .exec()
  }

  public async setKnowWordUser(
    telegram: number,
    wordId: Types.ObjectId,
  ) {
    return await this.userModel
      .findOneAndUpdate({ telegram }, { $addToSet: { knowsWords: wordId } }, { new: true })
      .exec()
  }

  public getAllRegisteredUsers() {
    return this.userModel.find({
      status: 'enabled',
      level: { $exists: true },
      language: { $exists: true },
      timezone: { $exists: true },
      notificationsStart: { $exists: true },
      notificationsEnd: { $exists: true },
    })
  }

  public async getUserById(id: Types.ObjectId): Promise<User> {
    return await this.userModel.findById(id)
  }

  public async getUserByTelegram(telegram: number): Promise<User> {
    return await this.userModel.findOne({ telegram })
  }

  public async disableUser(userId: Types.ObjectId): Promise<User> {
    return await this.userModel.findByIdAndUpdate(userId, { status: 'disabled' }).exec()
  }

  public async enableUser(userId: Types.ObjectId) {
    return await this.userModel.findByIdAndUpdate(userId, { status: 'enabled' }).exec()
  }

  public userIsDisabled(user: User): boolean {
    return user.status === 'disabled'
  }

  public async getCountUsers(): Promise<number> {
    return await this.userModel.countDocuments()
  }

  public async getUserCards(userId: Types.ObjectId): Promise<Card[]> {
    return await this.cardModel.find({ user: userId })
  }

  public async getUnlearnedRandomWordUser(userId: Types.ObjectId): Promise<Word> {
    const user = await this.getUserById(userId)
    const cards = await this.getUserCards(userId)
    const [word] = await this.wordsService.getRandomWordsWithout({
      language: user.language,
      level: user.level,
      size: 1,
      exclude: [
        ...user.knowsWords,
        ...cards.map((card) => card._id),
      ],
    })
    return word
  }

  public async getUsers(getUsersDTO: GetUsersDTO): Promise<{ count: number, items: User[] }> {
    const {
      offset = 0,
      limit = 10,
      sortDirection = 'desc',
      sortField = 'createdAt',
      search,
      language,
    } = getUsersDTO

    const match = {} as MatchType

    /**
     * Search by name
     */
    if (search) {
      match.username = {
        $regex: search,
        $options: 'i',
      }
    }

    /**
     * Filter by language
     */
    if (language) {
      match.language = {
        $in: language,
      }
    }

    const users = await this.userModel.aggregate([
      {
        $match: match,
      },
      {
        $sort: {
          [sortField]: sortDirection === 'asc' ? 1 : -1,
        },
      },
      {
        $lookup: {
          from: 'languages',
          localField: 'language',
          foreignField: '_id',
          as: 'language',
        },
      },
      {
        $lookup: {
          from: 'levels',
          localField: 'level',
          foreignField: '_id',
          as: 'level',
        },
      },
      {
        $lookup: {
          from: 'words',
          localField: 'dailyWords',
          foreignField: '_id',
          as: 'dailyWords',
        },
      },
      {
        $lookup: {
          from: 'words',
          localField: 'rememberWords',
          foreignField: '_id',
          as: 'rememberWords',
        },
      },
      {
        $unwind: {
          path: '$language',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $unwind: {
          path: '$level',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $project: {
          telegram: false,
        },
      },
      {
        $group: {
          _id: null,
          count: {
            $sum: 1,
          },
          items: {
            $push: '$$ROOT',
          },
        },
      },
      {
        $project: {
          _id: false,
          count: 1,
          items: {
            $slice: ['$items', offset, limit],
          },
        },
      },
    ])

    if (!users.length) {
      return {
        count: 0,
        items: [],
      }
    }

    return users[0]
  }
}
