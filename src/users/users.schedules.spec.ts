import { Test, TestingModule } from '@nestjs/testing'
import moment from 'moment'
import { forwardRef } from '@nestjs/common'
import sinon from 'sinon'
import { MongooseModule } from '@nestjs/mongoose'
import { DatabaseTestModule } from '../database-test.module'
import { UsersService } from './users.service'
import { UserSchema } from './schemas/user.schema'
import { UsersSchedules } from './users.schedules'
import { CardSchema } from './schemas/card.schema'
import { WordsModule } from '../words/words.module'
import { TelegramBotModule } from '../telegram-bot/telegram-bot.module'
import { CreateUserDTO } from './dto/create-user.dto'

describe('CardsService', () => {
  const createUserDTO: CreateUserDTO = {
    telegram: 2,
    username: 'test',
  }
  let usersService: UsersService
  let usersSchedules: UsersSchedules

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        DatabaseTestModule,
        MongooseModule.forFeature([{ name: 'Card', schema: CardSchema }]),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
        WordsModule,
        forwardRef(() => TelegramBotModule),
      ],
      providers: [
        UsersService,
        UsersSchedules,
      ],
    }).compile()

    usersService = module.get<UsersService>(UsersService)
    usersSchedules = module.get<UsersSchedules>(UsersSchedules)
  })

  it('should be defined', () => {
    expect(usersService).toBeDefined()
    expect(usersSchedules).toBeDefined()
  })

  describe('startSchedule', () => {
    it('should add new schedule in the common list', async () => {
      const user = await usersService.createUser(createUserDTO)
      await usersService.setNotificationsStartUser(user.telegram, '10:00')
      await usersService.setNotificationsEndUser(user.telegram, '22:00')
      const registeredUser = await usersService.setTimezoneUser(user.telegram, 'Asia/Yakutsk')

      usersSchedules.startSchedule(registeredUser)

      expect(usersSchedules.schedules.length).toEqual(1)
    })
  })

  describe('stopSchedule', () => {
    it('should remove schedule from the common list', async () => {
      const user = await usersService.createUser(createUserDTO)
      await usersService.setNotificationsStartUser(user.telegram, '10:00')
      await usersService.setNotificationsEndUser(user.telegram, '22:00')
      const registeredUser = await usersService.setTimezoneUser(user.telegram, 'Asia/Yakutsk')

      usersSchedules.startSchedule(registeredUser)
      expect(usersSchedules.schedules.length).toEqual(1)

      usersSchedules.stopSchedule(registeredUser)
      expect(usersSchedules.schedules.length).toEqual(0)
    })
  })

  describe('cron tick', () => {
    it('Should call 3 times a per day - 10,14,18', async () => {
      const user = await usersService.createUser(createUserDTO)
      await usersService.setNotificationsStartUser(user.telegram, '10:00')
      await usersService.setNotificationsEndUser(user.telegram, '22:00')
      const registeredUser = await usersService.setTimezoneUser(user.telegram, 'Asia/Yakutsk')
      const clock = sinon.useFakeTimers(moment('2019-12-13 00:00', 'YYYY-MM-DD HH:mm').toDate())

      usersSchedules.startSchedule(registeredUser)
      const hours: number[] = []
      const callback = jest.fn(() => hours.push(moment().get('hour')))

      usersSchedules.schedules[0].task.addCallback(callback)

      clock.tick(moment.duration(1, 'days').asMilliseconds())
      expect(callback).toBeCalledTimes(3)
      expect(hours).toEqual([10, 14, 18])
    })

    it('Should call 5 times a per day - 6,10,14,18,22', async () => {
      const user = await usersService.createUser(createUserDTO)
      await usersService.setNotificationsStartUser(user.telegram, '06:00')
      await usersService.setNotificationsEndUser(user.telegram, '23:00')
      const registeredUser = await usersService.setTimezoneUser(user.telegram, 'Asia/Yakutsk')
      const clock = sinon.useFakeTimers(moment('2019-12-13 00:00', 'YYYY-MM-DD HH:mm').toDate())

      usersSchedules.startSchedule(registeredUser)

      const hours: number[] = []
      const callback = jest.fn(() => hours.push(moment().get('hour')))
      usersSchedules.schedules[0].task.addCallback(callback)

      clock.tick(moment.duration(1, 'days').asMilliseconds())
      expect(callback).toBeCalledTimes(5)
      expect(hours).toEqual([6, 10, 14, 18, 22])
    })
  })
})
