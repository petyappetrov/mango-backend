import { Model, Types } from 'mongoose'
import { Injectable } from '@nestjs/common'
import moment from 'moment'
import { InjectModel } from '@nestjs/mongoose'
import { Card } from './interfaces/card.interface'
import { CreateCardDTO } from './dto/create-card.dto'

@Injectable()
export class CardsService {
  intervals: number[] = [1, 3, 8, 17, 32, 63, 127, 254]
  confidenceSteps: number[] = [-3, -1, 1]
  easyBonus: number = 0.3
  constructor(@InjectModel('Card') private readonly cardModel: typeof Model) {}

  public async createCard(createCardDTO: CreateCardDTO): Promise<Card> {
    return await this.cardModel.create(createCardDTO)
  }

  public async getCountNewCards(userId: Types.ObjectId): Promise<number> {
    const start = moment().startOf('day')
    const end = moment().endOf('day')

    return await this.cardModel.countDocuments({
      user: userId,
      nextReviewDate: {
        $gte: start,
        $lte: end,
      },
    })
  }

  public async getCardsOfToday(userId: Types.ObjectId): Promise<Card[]> {
    const start = moment().startOf('day')
    const end = moment().endOf('day')
    return await this.cardModel.find({
      user: userId,
      nextReviewDate: {
        $gte: start,
        $lte: end,
      },
    }).sort({ nextReviewDate: 1 })
  }

  public async getCardById(cardId: Types.ObjectId): Promise<Card> {
    return await this.cardModel.findById(cardId)
  }

  public async forgetCard(cardId: Types.ObjectId): Promise<Card> {
    return await this.cardModel.findByIdAndUpdate(cardId, { nextReviewDate: new Date() }, { new: true }).exec()
  }

  public async setNextLevelLearningDateCard(
    cardId: Types.ObjectId,
    confidenceIndex: 0 | 1 | 2,
  ): Promise<Card> {
    const card = await this.cardModel.findById(cardId)
    const calculatedValues = this.calculateIntervals(card, confidenceIndex)

    return this.cardModel.findByIdAndUpdate(card._id, calculatedValues, { new: true })
  }

  private calculateIntervals(
    card: Card,
    confidenceIndex: 0 | 1 | 2,
  ) {
    const confidence = this.confidenceSteps[confidenceIndex]

    const nextReviewDate = (() => {
      /**
       * Add days if user correct answered
       */
      if (confidence === 1) {
        return moment().add(this.intervals[card.intervalIndex], 'days')
      }

      return moment().add(this.intervals[0], 'days')
    })()

    /**
     * Get next interval index from confidence
     */
    const nextIntervalIndex = Math.min(
      card.intervalIndex + confidence,
      this.intervals.length - 1,
    )

    return {
      nextReviewDate: nextReviewDate.toDate(),
      intervalIndex: Math.max(0, nextIntervalIndex),
      isFinish: nextIntervalIndex === this.intervals.length - 1,
    }
  }
}
