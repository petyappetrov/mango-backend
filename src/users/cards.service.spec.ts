import { Test, TestingModule } from '@nestjs/testing'
import { Types } from 'mongoose'
import moment from 'moment'
import sinon from 'sinon'
import { MongooseModule } from '@nestjs/mongoose'
import { CardsService } from './cards.service'
import { CardSchema } from './schemas/card.schema'
import { DatabaseTestModule } from '../database-test.module'
import { CreateCardDTO } from './dto/create-card.dto'

describe('CardsService', () => {
  const createCardDTO: CreateCardDTO = {
    word: Types.ObjectId('5de1389740e01716b8686417'),
    user: Types.ObjectId('5dfbb4dca389ed0f28d14d77'),
  }
  let cardsService: CardsService
  let clock: sinon.SinonFakeTimers

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        DatabaseTestModule,
        MongooseModule.forFeature([{ name: 'Card', schema: CardSchema }]),
      ],
      providers: [
        CardsService,
      ],
    }).compile()

    cardsService = module.get<CardsService>(CardsService)

    clock = sinon.useFakeTimers(new Date())
  })

  it('should be defined', () => {
    expect(cardsService).toBeDefined()
  })

  describe('createCard', () => {
    it('should return a new entity', async () => {
      const card = await cardsService.createCard(createCardDTO)
      expect(card.word).toEqual(createCardDTO.word)
      expect(card.user).toEqual(createCardDTO.user)
    })
    it('should return default values', async () => {
      const card = await cardsService.createCard(createCardDTO)
      expect(card.isFinish).toBeFalsy()
      expect(card.intervalIndex).toEqual(0)
      expect(moment(card.nextReviewDate).get('days')).toEqual(moment().get('days'))
    })
  })

  describe('getCountNewCards', () => {
    it('should return count card of today', async () => {
      await cardsService.createCard(createCardDTO)
      await cardsService.createCard(createCardDTO)
      await cardsService.createCard(createCardDTO)
      const count = await cardsService.getCountNewCards(Types.ObjectId('5dfbb4dca389ed0f28d14d77'))
      expect(count).toEqual(3)
    })
  })

  describe('getCardsOfToday', () => {
    it('should return cards of today', async () => {
      await cardsService.createCard(createCardDTO)
      await cardsService.createCard(createCardDTO)
      const cards = await cardsService.getCardsOfToday(Types.ObjectId('5dfbb4dca389ed0f28d14d77'))
      expect(cards.length).toEqual(2)
    })
  })

  describe('getCardById', () => {
    it('should return card by id', async () => {
      const card = await cardsService.createCard(createCardDTO)
      const foundedCard = await cardsService.forgetCard(card._id)
      expect(foundedCard).toBeTruthy()
    })
  })

  describe('forgetCard', () => {
    it('should change nextReviewDate', async () => {
      const card = await cardsService.createCard(createCardDTO)
      const updatedCard = await cardsService.forgetCard(card._id)
      expect(updatedCard.nextReviewDate).not.toEqual(card.nextReviewDate)
    })
  })

  describe('setNextLevelLearningDateCard', () => {
    it('should change next date after easy', async () => {
      const card = await cardsService.createCard(createCardDTO)
      const updatedCard = await cardsService.setNextLevelLearningDateCard(card._id, 2)
      expect(card.nextReviewDate).not.toEqual(updatedCard.nextReviewDate)
    })

    it('should change next date after hard', async () => {
      const card = await cardsService.createCard(createCardDTO)
      const updatedCard = await cardsService.setNextLevelLearningDateCard(card._id, 1)
      expect(card.nextReviewDate).not.toEqual(updatedCard.nextReviewDate)
    })

    it('should change next date after very hard', async () => {
      const card = await cardsService.createCard(createCardDTO)
      const updatedCard = await cardsService.setNextLevelLearningDateCard(card._id, 0)
      expect(card.nextReviewDate).not.toEqual(updatedCard.nextReviewDate)
    })

    it('should change next date on second interval', async () => {
      const card = await cardsService.createCard(createCardDTO)
      const todayCard = await cardsService.setNextLevelLearningDateCard(card._id, 2)

      expect(todayCard.intervalIndex).toEqual(1)
      expect(moment(todayCard.nextReviewDate).subtract(1, 'days').get('days')).toEqual(moment().get('days'))

      clock.tick(moment.duration(1, 'days').asMilliseconds())
      const tomorrowCard = await cardsService.setNextLevelLearningDateCard(card._id, 2)

      expect(tomorrowCard.intervalIndex).toEqual(2)
      expect(moment(tomorrowCard.nextReviewDate).subtract(3, 'days').get('days')).toEqual(moment().get('days'))
    })

    it('should change next date on third interval', async () => {
      const card = await cardsService.createCard(createCardDTO)
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(1, 'days').asMilliseconds())
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(3, 'days').asMilliseconds())
      const updatedCard = await cardsService.setNextLevelLearningDateCard(card._id, 2)

      expect(updatedCard.intervalIndex).toEqual(3)
      expect(moment(updatedCard.nextReviewDate).subtract(8, 'days').get('days')).toEqual(moment().get('days'))
    })

    it('should change next date on fourth interval', async () => {
      const card = await cardsService.createCard(createCardDTO)
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(1, 'days').asMilliseconds())
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(3, 'days').asMilliseconds())
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(8, 'days').asMilliseconds())
      const updatedCard = await cardsService.setNextLevelLearningDateCard(card._id, 2)

      expect(updatedCard.intervalIndex).toEqual(4)
      expect(moment(updatedCard.nextReviewDate).subtract(17, 'days').get('days')).toEqual(moment().get('days'))
    })

    it('should change next date on fifth interval', async () => {
      const card = await cardsService.createCard(createCardDTO)
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(1, 'days').asMilliseconds())
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(3, 'days').asMilliseconds())
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(8, 'days').asMilliseconds())
      await cardsService.setNextLevelLearningDateCard(card._id, 2)

      clock.tick(moment.duration(17, 'days').asMilliseconds())
      const updatedCard = await cardsService.setNextLevelLearningDateCard(card._id, 2)

      expect(updatedCard.intervalIndex).toEqual(5)
      expect(moment(updatedCard.nextReviewDate).subtract(32, 'days').get('days')).toEqual(moment().get('days'))
    })
  })
})
