import * as mongoose from 'mongoose'

export const CardSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  word: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Word',
  },
  nextReviewDate: {
    type: Date,
    default: new Date(),
  },
  intervalIndex: {
    type: Number,
    default: 0,
  },
  isFinish: {
    type: Boolean,
    default: false,
  },
}, {
  versionKey: false,
  timestamps: true,
})
