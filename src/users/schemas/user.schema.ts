import * as mongoose from 'mongoose'

export const UserSchema = new mongoose.Schema({
  /**
   * Username of user
   * Taken from telegram, do not change and do not create manually
   */
  username: String,

  /**
   * First name of user
   */
  firstName: String,

  /**
   * Telegram id of user
   */
  telegram: {
    type: String,
    required: true,
    unique: true,
  },

  /**
   * Timezone of user location
   */
  timezone: String,

  /**
   * Language of user learning
   */
  language: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Language',
  },

  /**
   * Level of language
   */
  level: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Level',
  },

  /**
   * Start of notifications
   */
  notificationsStart: String,

  /**
   * End of notifications
   */
  notificationsEnd: String,

  /**
   * Native language of user
   * This field will need in the future if we add other languages
   */
  nativeLanguage: {
    type: String,
    default: 'ru',
  },

  /**
   * User status
   * If user stopped telegram bot, need change status to disabled
   */
  status: {
    type: String,
    enum: ['disabled', 'enabled'],
    default: 'disabled',
  },

  /**
   * Interval hours for send message
   */
  interval: {
    type: Number,
    default: 4,
  },

  /**
   * Limit new cards per day
   */
  limitPerDay: {
    type: Number,
    default: 5,
  },

  /**
   * Here put the words of the user knows
   */
  knowsWords: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Word',
  }],
}, {
  versionKey: false,
  timestamps: true,
})
