import { Injectable, OnModuleInit, Inject, forwardRef } from '@nestjs/common'
import { Types } from 'mongoose'
import { CronJob } from 'cron'
import moment from 'moment-timezone'
import { UsersService } from '../users/users.service'
import { User } from '../users/interfaces/user.interface'
import { CardsChooseCommand } from '../telegram-bot/commands/cards-choose.command'
import { WordCommand } from '../telegram-bot/commands/word.command'

interface Schedule {
  task: CronJob
  userId: Types.ObjectId
}

@Injectable()
export class UsersSchedules implements OnModuleInit {
  public schedules: Schedule[] = []
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly cardsChooseCommand: CardsChooseCommand,

  ) {}

  onModuleInit() {
    this.initializeSchedules()
  }

  private async initializeSchedules() {
    const users: User[] = await this.usersService.getAllRegisteredUsers()

    /**
     * Initialization of push-messages
     */
    users.forEach(this.startSchedule.bind(this))
  }

  public startSchedule(user: User) {
    if (
      !user.notificationsStart ||
      !user.notificationsEnd ||
      !user.timezone
    ) {
      return
    }

    if (this.scheduleAlreadyExist(user)) {
      /**
       * Modify schedule
       */
      this.schedules = this.schedules.map((schedule) => {
        if (schedule.userId.equals(user._id)) {
          /**
           * Destroy old task
           */
          schedule.task.stop()
          return this.createSchedule(user)
        }
        return schedule
      })
    } else {
      /**
       * Create new schedule
       */
      this.schedules = [...this.schedules, this.createSchedule(user)]
    }
  }

  public stopSchedule(user: User) {
    if (this.scheduleAlreadyExist(user)) {
      const schedule = this.schedules.find((s) => s.userId.equals(user._id))

      /**
       * Destroy task
       */
      schedule.task.stop()

      /**
       * Remove schedule from common store
       */
      this.schedules = this.schedules.filter((s) => !s.userId.equals(user._id))
    }
  }

  private createSchedule(user: User) {
    const cronValue = this.getCronValue(user)
    const task = new CronJob({
      cronTime: cronValue,
      onTick: () => this.onTickHandle(user._id),
      start: true,
      timeZone: user.timezone,
    })

    return {
      task,
      userId: user._id,
    }
  }

  private async onTickHandle(userId: Types.ObjectId) {
    const user = await this.usersService.getUserById(userId)
    const start = parseInt(user.notificationsStart.split(':')[0], 10)

    /**
     * Take current hours using local date and timezone
     */
    const currentHours = parseInt(moment().tz(user.timezone).format('HH'), 10)

    if (currentHours === start) {
      /**
       * Send first message of the choose new words
       */
      this.cardsChooseCommand.startChooseWordsCommand(user)
    } else {
      // TODO: Send most difficult words
      // this.wordCommand.sendWordToUser(user)
    }
  }

  private getCronValue({ notificationsStart, notificationsEnd, interval = 4 }: User) {
    const start: number = parseInt(notificationsStart.split(':')[0], 10)
    const end: number = parseInt(notificationsEnd.split(':')[0], 10)
    const hours: number[] = []

    let i = start
    while (i < end) {
      hours.push(i)
      i += interval
    }

    return `0 ${hours.join(',')} * * *`
  }

  private scheduleAlreadyExist(user: User) {
    return (
      this.schedules.length &&
      this.schedules.some((schedule) => schedule.userId.equals(user._id))
    )
  }
}
