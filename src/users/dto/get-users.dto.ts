export class GetUsersDTO {
  readonly sortDirection?: 'asc' | 'desc'
  readonly sortField?: 'word' | 'createdAt'
  readonly search?: string
  readonly language?: string
  readonly offset?: number = 0
  readonly limit?: number = 10
}
