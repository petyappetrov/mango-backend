import { IsNotEmpty } from 'class-validator'
import { Types } from 'mongoose'

export class CreateCardDTO {
  readonly user: Types.ObjectId
  readonly word: Types.ObjectId
}
