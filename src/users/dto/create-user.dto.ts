import { IsNotEmpty } from 'class-validator'

export class CreateUserDTO {
  readonly username?: string

  @IsNotEmpty()
  readonly telegram: number

  readonly firstName?: string

  readonly nativeLanguage?: string

  readonly language?: string
}
