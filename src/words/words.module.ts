import { MongooseModule } from '@nestjs/mongoose'
import { Module } from '@nestjs/common'
import { WordSchema } from './schemas/word.schema'
import { WordsService } from './words.service'
import { WordsController } from './words.controller'
import { LevelsModule } from '../levels/levels.module'
import { LanguagesModule } from '../languages/languages.module'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Word', schema: WordSchema }]),
    LevelsModule,
    LanguagesModule,
  ],
  providers: [WordsService],
  controllers: [WordsController],
  exports: [WordsService],
})
export class WordsModule {}
