import { Controller, Post, Body, Response, Param, Delete, Put, Get, UseGuards, Query } from '@nestjs/common'
import { AuthGuard, Type } from '@nestjs/passport'
import { CreateWordDTO } from './dto/create-word.dto'
import { WordsService } from './words.service'
import { Response as ResponseType } from 'express'
import { UpdateWordDTO } from './dto/update-word.dto'
import { GetWordsDTO } from './dto/get-words.dto'
import { Types } from 'mongoose'

@UseGuards(AuthGuard('jwt'))
@Controller('words')
export class WordsController {
  constructor(private wordsService: WordsService) {}

  @Post()
  public async createWord(
    @Body() createWordDTO: CreateWordDTO,
    @Response() response: ResponseType,
  ) {
    const word = await this.wordsService.createWord(createWordDTO)
    return response.status(200).json(word)
  }

  @Put(':id')
  public async updateWord(
    @Param('id') id: string,
    @Body() updateWordDTO: UpdateWordDTO,
    @Response() response: ResponseType,
  ) {
    const word = await this.wordsService.updateWord(id, updateWordDTO)
    return response.status(200).json(word)
  }

  @Delete(':id')
  public async removeWord(
    @Param('id') id: string,
    @Response() response: ResponseType,
  ) {
    const word = await this.wordsService.removeWord(id)
    return response.status(200).json(word)
  }

  @Get()
  public async getWords(
    @Response() response: ResponseType,
    @Query() getWordsDTO: GetWordsDTO,
  ) {
    const words = await this.wordsService.getWords(getWordsDTO)
    return response.status(200).json(words)
  }

  @Get(':value')
  public async getWord(
    @Response() response: ResponseType,
    @Param('value') value: string,
  ) {
    const word = await this.wordsService.getWordByValue(value)
    return response.status(200).json(word)
  }

  @Delete()
  async removeWords(
    @Response() response: ResponseType,
    @Body('ids') ids: Types.ObjectId[],
  ) {
    const words = await this.wordsService.removeWords(ids)
    return response.status(200).json(words)
  }
}
