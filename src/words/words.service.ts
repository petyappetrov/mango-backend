import { Model, Types } from 'mongoose'
import { Injectable, OnApplicationBootstrap } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Word } from './interfaces/word.interface'
import { CreateWordDTO } from './dto/create-word.dto'
import { UpdateWordDTO } from './dto/update-word.dto'
import { GetWordsDTO } from './dto/get-words.dto'
import WordFixtures from './schemas/word.fixtures.json'
import { LevelsService } from '../levels/levels.service'
import { LanguagesService } from '../languages/languages.service'

interface MatchType {
  language?: Types.ObjectId
  word?: object
  level?: Types.ObjectId
  _id: object
}

@Injectable()
export class WordsService implements OnApplicationBootstrap {
  constructor(
    @InjectModel('Word') private readonly wordModel: Model<Word>,
    private readonly levelsService: LevelsService,
    private readonly languagesService: LanguagesService,
  ) {}

  async onApplicationBootstrap() {
    const words = await this.wordModel.find({})
    if (!words.length) {
      const correctedWords = await Promise.all(WordFixtures.map(async (word: any) => {
        const level = await this.levelsService.getLevelByCode(word.level)
        const language = await this.languagesService.getLanguageByCode(word.language)
        if (level) {
          return {
            ...word,
            level: level._id,
            language: language._id,
          }
        }
        return word
      }))
      this.wordModel.create(correctedWords)
    }
  }

  public async createWord(createWordDTO: CreateWordDTO): Promise<Word> {
    const word = await this.wordModel.create(createWordDTO)
    await word
      .populate('language')
      .execPopulate()
    return word
  }

  public async updateWord(
    id: string,
    updateWordDTO: UpdateWordDTO,
  ): Promise<Word> {
    return await this.wordModel.findByIdAndUpdate(id, updateWordDTO).exec()
  }

  public async removeWord(id: string): Promise<Word> {
    return await this.wordModel.findByIdAndRemove(id).exec()
  }

  public async removeWords(ids: Types.ObjectId[]): Promise<Word[]> {
    return await Promise.all(
      ids.map((id) => this.wordModel.findByIdAndRemove(id)),
    )
  }

  public async getWordById(id: Types.ObjectId): Promise<Word> {
    return await this.wordModel.findById(id)
  }

  public async getRandomWordsWithout({
    language,
    level,
    exclude = [],
    size = 1,
  }: {
    exclude?: Types.ObjectId[]
    language?: Types.ObjectId
    level?: Types.ObjectId,
    size?: number,
  }): Promise<Word[]> {
    return await this.wordModel
      .aggregate([
        {
          $match: {
            _id: {
              $nin: exclude,
            },
            language,
            level,
          },
        },
        {
          $sample: { size },
        },
      ])
      .exec()
  }

  public async saveVoice(wordId: Types.ObjectId, voice: string) {
    return await this.wordModel
      .findByIdAndUpdate(wordId, { voice }, { new: true })
      .exec()
  }

  public async getWordByValue(word: string): Promise<Word> {
    return await this.wordModel.findOne({ word }).populate(['language', 'level']).exec()
  }

  public async getWords(
    getWordsDTO: GetWordsDTO = {},
  ): Promise<{ items: Word[]; count: number }> {
    const {
      offset = '0',
      limit = '30',
      sortDirection = 'desc',
      sortField = 'createdAt',
      search,
      language,
      level,
    } = getWordsDTO

    const match = {} as MatchType

    /**
     * Search by name
     */
    if (search) {
      match.word = {
        $regex: search,
        $options: 'i',
      }
    }

    /**
     * Filter by language
     */
    if (language) {
      match.language = Types.ObjectId(language)
    }

    /**
     * Filter by level
     */
    if (level) {
      match.level = Types.ObjectId(level)
    }

    const words = await this.wordModel.aggregate([
      {
        $match: match,
      },
      {
        $sort: {
          [sortField]: sortDirection === 'asc' ? 1 : -1,
        },
      },
      {
        $lookup: {
          from: 'languages',
          localField: 'language',
          foreignField: '_id',
          as: 'language',
        },
      },
      {
        $lookup: {
          from: 'levels',
          localField: 'level',
          foreignField: '_id',
          as: 'level',
        },
      },
      {
        $unwind: {
          path: '$language',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $unwind: {
          path: '$level',
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $group: {
          _id: null,
          count: {
            $sum: 1,
          },
          items: {
            $push: '$$ROOT',
          },
        },
      },
      {
        $project: {
          _id: false,
          count: 1,
          items: {
            $slice: ['$items', parseInt(offset, 10), parseInt(limit, 10)],
          },
        },
      },
    ])

    if (!words.length) {
      return {
        count: 0,
        items: [],
      }
    }

    return words[0]
  }
}
