import { Document } from 'mongoose'

export interface Word extends Document {
  word: string
  translate: string
  transcription: string
  type:
    | 'verb'
    | 'noun'
    | 'preposition'
    | 'adjective'
    | 'modal verb'
    | 'adverb'
    | 'conjunction'
    | 'pronoun'
    | 'interjection'
    | 'article'
  language: string
  level: string
  examples?: string[]
  voice?: string
}
