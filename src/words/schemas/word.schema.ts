import * as mongoose from 'mongoose'

export const WordSchema = new mongoose.Schema(
  {
    word: {
      type: String,
      required: true,
    },
    translate: {
      type: String,
      required: true,
    },
    transcription: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
      enum: [
        'verb',
        'noun',
        'preposition',
        'adjective',
        'modal verb',
        'adverb',
        'conjunction',
        'pronoun',
        'interjection',
        'article',
      ],
    },
    language: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Language',
      required: true,
    },
    level: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Level',
      required: true,
    },
    examples: [String],
    voice: String,
  },
  {
    versionKey: false,
  },
)
