export class UpdateWordDTO {
  readonly word?: string
  readonly translate?: string
  readonly language?: string
  readonly level?: string
  readonly example?: string
}
