import { Schema } from 'mongoose'
import { IsNotEmpty } from 'class-validator'

export class CreateWordDTO {
  @IsNotEmpty()
  readonly word: string

  @IsNotEmpty()
  readonly translate: string

  @IsNotEmpty()
  readonly transcription: string

  @IsNotEmpty()
  readonly type:
    | 'verb'
    | 'noun'
    | 'preposition'
    | 'adjective'
    | 'modal verb'
    | 'adverb'
    | 'conjunction'
    | 'pronoun'
    | 'interjection'
    | 'article'

  @IsNotEmpty()
  readonly language: Schema.Types.ObjectId

  @IsNotEmpty()
  readonly level: Schema.Types.ObjectId

  readonly examples?: string[]
}
