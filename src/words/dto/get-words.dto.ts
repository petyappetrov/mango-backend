export class GetWordsDTO {
  readonly sortDirection?: 'asc' | 'desc'
  readonly sortField?: 'word' | 'createdAt'
  readonly search?: string
  readonly language?: string
  readonly level?: string
  readonly offset?: string
  readonly limit?: string
}
