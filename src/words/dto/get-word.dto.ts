import { Types } from 'mongoose'

export class GetWordDTO {
  exclude?: Types.ObjectId[]
  language?: Types.ObjectId
  level?: Types.ObjectId
}
