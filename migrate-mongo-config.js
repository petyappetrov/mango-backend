const dotenv = require('dotenv')
const fs = require('fs')

const getDBUrlFromEnvFile = () => {
  const envFilePath = process.env.NODE_ENV === 'production' ? '.env' : '.env.development'
  const envConfig = dotenv.parse(fs.readFileSync(envFilePath))
  const host = envConfig['DB_HOST']
  const port = envConfig['DB_PORT']
  const name = envConfig['DB_NAME']
  const username = envConfig['DB_USERNAME']
  const password = envConfig['DB_PASSWORD']
  return `mongodb://${username}:${password}@${host}:${port}/${name}`
}

const config = {
  mongodb: {
    url: getDBUrlFromEnvFile(),
    databaseName: 'mango',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  migrationsDir: 'migrations',
  changelogCollectionName: 'changelog',
}

module.exports = config
